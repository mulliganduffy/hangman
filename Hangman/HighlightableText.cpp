#include "stdafx.h"
#include "HighlightableText.h"

HighlightableText::HighlightableText(std::string rawString)
{
	m_rawString = rawString;
	AddHighlightParameter(0, rawString.length(), 7);
}

HighlightableText::HighlightableText(std::string rawString, int highlightStartPoint, int highlightLength, unsigned short highlightColor)
{
	m_rawString = rawString;
	AddHighlightParameter(0, rawString.length(), 7);
	AddHighlightParameter(highlightStartPoint, highlightLength, highlightColor);
}

std::string HighlightableText::GetRawString() const
{
	return m_rawString;
}

unsigned short HighlightableText::GetColorForChar(const size_t& charPosition) const
{
	return m_highlightParameters.at(charPosition);
}

void HighlightableText::AddHighlightParameter(size_t highlightStartPoint, size_t highlightLength, unsigned short highlightColor)
{
	//Go through the specified portion of the raw string
	for (size_t i = highlightStartPoint; i < highlightStartPoint + highlightLength; i++) {
		m_highlightParameters[i] = highlightColor;
	}
}
