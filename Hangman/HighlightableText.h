/* This class functions as a string with parameters specifying which parts of the string
 should be colored, and into which color */
#pragma once
#ifndef HIGHLIGHTABLE_TEXT_H
#define HIGHLIGHTABLE_TEXT_H

#include <map>
#include <string>

class HighlightableText
{
public:
	HighlightableText(std::string rawString);
	HighlightableText(std::string rawString, int highlightStartPoint, int highlightLength, unsigned short highlightColor);

	std::string GetRawString() const;
	unsigned short GetColorForChar(const size_t& charPosition) const;

	void AddHighlightParameter(size_t highlightStartPoint, size_t highlightLength, unsigned short highlightColor);

private:
	std::string m_rawString;
	std::map<size_t, unsigned short> m_highlightParameters;
};

#endif // !HIGHLIGHTABLE_TEXT_H
