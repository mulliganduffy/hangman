﻿#include "stdafx.h"
#include "HangmanDrawer.h"
#include <iostream>

void hangDraw::DrawFullHangmanPicture(const HangmanGameState& state)
{
	DrawHangmanPost();
	DrawHangmanMan(state);
}

void hangDraw::DrawHangmanPost()
{
	std::cout << "__________    \n";
	std::cout << "|        |    \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
	std::cout << "|             \n";
}

void hangDraw::DrawHangmanMan(const HangmanGameState& state)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

	//Do we print the head?
	if ((state & HangmanGameState::Head) == HangmanGameState::Head) {
		DrawBodyPart(8, 2, 3, 2);
	}

	//Do we print the torso?
	if ((state & HangmanGameState::Torso) == HangmanGameState::Torso) {
		DrawBodyPart(9, 4, 1, 5);
	}

	//Do we print the left arm?
	if ((state & HangmanGameState::LeftArm) == HangmanGameState::LeftArm) {
		DrawBodyPart(5, 5, 4, 1);
	}

	//Do we print the right arm?
	if ((state & HangmanGameState::RightArm) == HangmanGameState::RightArm) {
		DrawBodyPart(10, 5, 4, 1);
	}

	//Do we print the left leg?
	if ((state & HangmanGameState::LeftLeg) == HangmanGameState::LeftLeg) {
		DrawBodyPart(8, 9, 1, 4);
	}

	//Do we print the right leg?
	if ((state & HangmanGameState::RightLeg) == HangmanGameState::RightLeg) {
		DrawBodyPart(10, 9, 1, 4);
	}
}

void hangDraw::DrawBodyPart(const SHORT & bufferXPos, const SHORT & bufferYPos, const SHORT & bufferWidth, const SHORT & bufferHeight)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

	CHAR_INFO *bodyBuffer;
	bodyBuffer = new CHAR_INFO[bufferWidth * bufferHeight];

	COORD bodyBufferSize = { bufferWidth, bufferHeight };
	COORD bodyBufferPosition = { 0, 0 };
	SMALL_RECT bodyWriteRegion = { bufferXPos, bufferYPos, bufferXPos + bufferWidth, bufferYPos + bufferHeight };
	for (int i = 0; i < (bufferWidth * bufferHeight); i++) {
		bodyBuffer[i].Char.AsciiChar = static_cast<char>(219);//'O';
		bodyBuffer[i].Attributes = FOREGROUND_INTENSITY;
	}

	WriteConsoleOutputA(handle, bodyBuffer, bodyBufferSize, bodyBufferPosition, &bodyWriteRegion);

	delete[] bodyBuffer;
}
