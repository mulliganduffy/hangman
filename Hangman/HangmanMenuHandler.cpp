#include "stdafx.h"
#include "HangmanMenuHandler.h"
#include <Windows.h>

HangmanMenuHandler::HangmanMenuHandler()
{
}

void HangmanMenuHandler::ProcessInput(const TCHAR& input, bool& bPlayGame, bool& bStillInApp)
{
	if ('0' < input && input <= '9') {
		if (m_activeMenuSectionID == "Main Menu") {
			switch (input) {
			case '1':
				SetActiveSection("Game Menu");
				bPlayGame = true;
				break;
			case '2':
				SetActiveSection("Quit Menu");
				break;
			}
		} else if (m_activeMenuSectionID == "Quit Menu") {
			switch (input) {
			case '1':
				bStillInApp = false;
				break;
			case '2':
				SetActiveSection("Main Menu");
				break;
			}
		}
	}
}
