/* This is the most important class in our application. It picks a random word for the player to guess,
 displays the word to them as correct letters get revealed,
 and tracks the player's progress in response to their guesses*/
#pragma once
#ifndef HANGMAN_SESSION_H
#define HANGMAN_SESSION_H

#include "HangmanGameState.h"
#include "Player.h"
#include <unordered_map>
#include <string>

class HangmanSession
{
public:
	HangmanSession();

	std::string GetSessionWord() const;
	std::string GetWordGuess() const;
	std::string GetResponseMessage() const;
	HangmanGameState GetGameState() const;

	void Reset();
	void ChooseWord();

	void ProcessGuess(std::string guessWord);
	void ProcessGuess(char guess);
	void UpdateGameState();
	void CreateListOfGivenGuesses();

	bool IsGameFinished() const;
	void EndSession(Player& player, bool& bHasWon);

private:
	std::string m_sessionWord; //The word the player must guess
	std::string m_wordGuess; //How the session word will look to the player as they guess
	std::string m_responseMessage;
	HangmanGameState m_gameState;
	std::unordered_map<char, bool> m_givenGuesses;

	const HangmanGameState gameOver = (HangmanGameState::Head | HangmanGameState::Torso
		| HangmanGameState::LeftArm | HangmanGameState::RightArm
		| HangmanGameState::LeftLeg | HangmanGameState::RightLeg);
};
#endif // !HANGMAN_SESSION_H