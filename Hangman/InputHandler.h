// A namespace for taking in input
#pragma once
#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include <Windows.h>

namespace inputHandle {
	//https://stackoverflow.com/questions/41212646/get-key-press-in-windows-console (12:13 30/11/2017)
	CHAR getch(); //Get one char

}

#endif // !INPUT_HANDLER_H
