/* This file allows for more visuals for our application (i.e. more color or boxes)
 It is not a class but a namespace, there would not be a point in making it a class */
#pragma once
#ifndef STRING_FORMATTER_H
#define STRING_FORMATTER_H

#include "HighlightableText.h"
#include <Windows.h>
#include <vector>

namespace strFormat {
	//Structs
	struct BoxParameters {
		WORD borderColor;
		WORD backgroundColor;
		int padding;
		char borderChar;
	};
	const BoxParameters DEFAULT_BOX = { 7, 7, 1, '*' };

	//Highlights
	void PrintHighlightedString(const HANDLE& handle, const HighlightableText& text, const bool& bNewLine = true);
	void PrintHighlightedString(const HANDLE& handle, const std::vector<HighlightableText>& texts);

	//Box
	size_t GetLongestStringInVector(const std::vector<HighlightableText>& lines);

	//Box AND Highlights
	void PrintMultipleHighlightedTextsInsideBox(const HANDLE& handle, const std::vector<HighlightableText>& texts, const BoxParameters& box = DEFAULT_BOX);
}

#endif // !STRING_FORMATTER_H

