#include "stdafx.h"
#include "Player.h"

Player::Player()
	:Player("Noname")
{
}

Player::Player(std::string name)
{
	m_name = name;
	m_wins = 0;
	m_loses = 0;
}

void Player::SetName(std::string name)
{
	m_name = name;
}

std::string Player::GetName()
{
	return m_name;
}

void Player::SetWins(int wins)
{
	m_wins = wins;
}

int Player::GetWins()
{
	return m_wins;
}

void Player::SetLoses(int loses)
{
	m_loses = loses;
}

int Player::GetLoses()
{
	return m_loses;
}

void Player::UpdateWinsAndLoses(bool bHasWon)
{
	if (bHasWon) {
		m_wins++;
	} else {
		m_loses++;
	}
}
