/* This represents the user playing the game. It contains their name,
and their score in Hangman */
#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include <string>

class Player
{
public:
	Player();
	Player(std::string name);

	void SetName(std::string name);
	std::string GetName();

	void SetWins(int wins);
	int GetWins();

	void SetLoses(int loses);
	int GetLoses();

	void UpdateWinsAndLoses(bool bHasWon);

private:
	std::string m_name;
	int m_wins;
	int m_loses;
};

#endif // !PLAYER_H



