/* This class will implement the menus for our Hangman game, inheriting from the base MenuHandler*/
#pragma once
#ifndef HANGMAN_MENU_HANDLER_H
#define HANGMAN_MENU_HANDLER_H

#include "MenuHandler.h"
#include "InputHandler.h"

class HangmanMenuHandler : public MenuHandler
{
public:
	HangmanMenuHandler();

	void ProcessInput(const TCHAR& input, bool& bPlayGame, bool& bStillInApp);
};

#endif // !HANGMAN_MENU_HANDLER_H

