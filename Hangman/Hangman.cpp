// Hangman.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "StringFormatter.h"
#include "InputHandler.h"
#include "HangmanMenuHandler.h"
#include "HangmanSession.h"
#include "HangmanDrawer.h"
#include "Player.h"
#include "HighlightableText.h"
#include <Windows.h>
#include <iostream>

HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
bool bStillInApp = true;
bool bPlayingGame = false;

HangmanMenuHandler hangmanMenu;
HangmanSession currentSession;
Player player;

void InitializeGame();
void BuildMenu();

// The loop of the game
void DrawMenu();
void TakeInMenuInput();
void DrawHangman();

void GuessALetter();
void GuessAWord();

void ShowResults(bool &bHasWon);

int main()
{
	InitializeGame();

	BuildMenu();
	system("cls");

	while (bStillInApp) {
		DrawMenu();
		TakeInMenuInput();
		system("cls"); // Might achieve this a different way
		if (bPlayingGame) {
			DrawHangman();
		}
	}

    return 0;
}

void InitializeGame()
{
	SetConsoleTitleA("Hangman");

	std::cout << "Enter your name\n> ";
	std::string playerName;
	std::getline(std::cin, playerName);
	player.SetName(playerName);
}

void BuildMenu()
{
	//Main menu
	std::string rawWelcomeMsg("Welcome, ");
	rawWelcomeMsg.append(player.GetName());
	rawWelcomeMsg.append(", to Hangman!");
	HighlightableText welcomeMsg(rawWelcomeMsg, 0, rawWelcomeMsg.length(), FOREGROUND_RED | FOREGROUND_BLUE);
	welcomeMsg.AddHighlightParameter(9, player.GetName().length(), 7);
	welcomeMsg.AddHighlightParameter(14 + player.GetName().length(), 7, 156);
	hangmanMenu.Add("Main Menu", welcomeMsg);

	hangmanMenu.Add("Main Menu", HighlightableText("What would you like to do?", 0, 26, FOREGROUND_RED | FOREGROUND_BLUE));
	hangmanMenu.Add("Main Menu", HighlightableText(" "));
	hangmanMenu.Add("Main Menu", HighlightableText("1) Play", 0, 7, FOREGROUND_RED | FOREGROUND_GREEN));
	hangmanMenu.Add("Main Menu", HighlightableText("2) Quit", 0, 7, FOREGROUND_RED | FOREGROUND_GREEN));

	//Quit menu
	hangmanMenu.Add("Quit Menu", HighlightableText("Are you sure you want to quit?", 0, 30, FOREGROUND_RED | FOREGROUND_BLUE));
	hangmanMenu.Add("Quit Menu", HighlightableText(" "));
	hangmanMenu.Add("Quit Menu", HighlightableText("1) Yes", 0, 6, FOREGROUND_RED | FOREGROUND_GREEN));
	hangmanMenu.Add("Quit Menu", HighlightableText("2) No", 0, 5, FOREGROUND_RED | FOREGROUND_GREEN));

	//Hangman game menu
	hangmanMenu.Add("Game Menu", HighlightableText("How would you like to guess?", 0, 30, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN));
	hangmanMenu.Add("Game Menu", HighlightableText("1) Letter", 0, 9, FOREGROUND_BLUE | FOREGROUND_GREEN));
	hangmanMenu.Add("Game Menu", HighlightableText("2) Word", 0, 7, FOREGROUND_BLUE | FOREGROUND_GREEN));
}

void DrawMenu()
{
	strFormat::BoxParameters menuBox = { 12, 56, 2, static_cast<char>(178) };
	strFormat::PrintMultipleHighlightedTextsInsideBox(handle, *hangmanMenu.GetActiveSection(), menuBox);
}

void TakeInMenuInput()
{
	TCHAR key = inputHandle::getch();

	if (hangmanMenu.GetActiveMenuSectionID() == "Game Menu") {
		// Take user to the appropriate place the enter their guess
		switch (key) {
		case '1':
			GuessALetter();
			break;
		case '2':
			GuessAWord();
			break;
		}

		// Has the game ended because of this guess
			// If it has, end the session
		if (currentSession.IsGameFinished()) {
			bool bHasWon = true; //Aussume the player has won until proven otherwise
			currentSession.EndSession(player, bHasWon);
			ShowResults(bHasWon);
			hangmanMenu.SetActiveSection("Main Menu");
			bPlayingGame = false;
		}
	} else {
		hangmanMenu.ProcessInput(key, bPlayingGame, bStillInApp);
	}
}

void DrawHangman()
{
	//Draw the state of the session, in the form of the traditional hangman picture
	hangDraw::DrawFullHangmanPicture(currentSession.GetGameState());
	//Draw the state of the player's guess of the session's chosen word
	strFormat::PrintHighlightedString(handle, HighlightableText(currentSession.GetWordGuess()));
    std::cout << currentSession.GetResponseMessage() << '\n';
}

void GuessALetter()
{
	strFormat::PrintHighlightedString(handle, HighlightableText("Enter your letter: "), false);

	char key = inputHandle::getch();

	currentSession.ProcessGuess(key);
}

void GuessAWord()
{
	strFormat::PrintHighlightedString(handle, HighlightableText("Enter your guess: "), false);
	std::string playerGuess;
	std::getline(std::cin, playerGuess);

	currentSession.ProcessGuess(playerGuess);
}

void ShowResults(bool &bHasWon)
{
	system("cls");
	std::vector<HighlightableText> resultsTexts;

	if (bHasWon) {
		resultsTexts.push_back(HighlightableText("Congratulations, you won!", 0, 25, FOREGROUND_GREEN | FOREGROUND_INTENSITY | BACKGROUND_BLUE));
	} else {
		resultsTexts.push_back(HighlightableText("Unfortunately, you have lost!", 0, 29, FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN));
	}

	//String for wins
	std::string rawWinMsg("You currently have ");
	rawWinMsg.append(std::to_string(player.GetWins()));
	rawWinMsg.append(" wins.");
	HighlightableText WinMsg(rawWinMsg, 0, rawWinMsg.length(), FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	WinMsg.AddHighlightParameter(19, std::to_string(player.GetWins()).length(), FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	resultsTexts.push_back(WinMsg);

	//String for loses
	std::string rawLoseMsg("You currently have ");
	rawLoseMsg.append(std::to_string(player.GetLoses()));
	rawLoseMsg.append(" loses.");
	HighlightableText LoseMsg(rawLoseMsg, 0, rawLoseMsg.length(), FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	LoseMsg.AddHighlightParameter(19, std::to_string(player.GetLoses()).length(), FOREGROUND_RED | FOREGROUND_INTENSITY);
	resultsTexts.push_back(LoseMsg);

	strFormat::BoxParameters resultsBox = {FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN, 0, 3, static_cast<char>(176)};
	strFormat::PrintMultipleHighlightedTextsInsideBox(handle, resultsTexts, resultsBox);
	Sleep(5000);
}
