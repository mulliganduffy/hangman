#include "stdafx.h"
#include "MenuHandler.h"


MenuHandler::MenuHandler()
{
}

void MenuHandler::Add(std::string menuSectionID, HighlightableText menuText)
{
	//Check if the specified menu section actually exists in the menu handler
	if (m_menuStash.count(menuSectionID) != 0) {
		m_menuStash[menuSectionID].push_back(menuText);
	} else {
		std::vector<HighlightableText> newMenuSection; //Create a new vector of strings
		newMenuSection.push_back(menuText); //Add the menu text into this vector
		m_menuStash[menuSectionID] = newMenuSection; //Add the vector into the menu stash
	}

	//In case it hasn't yet, set the active section to the last menuText's ID
	if (!pm_activeMenuSection) { 
		SetActiveSection(menuSectionID);
	}
}

bool MenuHandler::SetActiveSection(std::string menuSectionID)
{
	if (m_menuStash.count(menuSectionID) != 0) {
		pm_activeMenuSection = &m_menuStash[menuSectionID];
		m_activeMenuSectionID = menuSectionID;
		return true;
	}
	return false;
}

std::vector<HighlightableText>* MenuHandler::GetActiveSection() const
{
	return pm_activeMenuSection;
}

std::string MenuHandler::GetActiveMenuSectionID() const
{
	return m_activeMenuSectionID;
}
