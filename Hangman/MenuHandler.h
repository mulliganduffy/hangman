/* A base class that is capable of storing menus for an application 
If we want menus in an application, we would inherit from this for the specific task we need it to do*/
#pragma once
#ifndef MENU_HANDLER_H
#define MENU_HANDLER_H

#include "HighlightableText.h"
#include <map>
#include <vector>

class MenuHandler
{
public:
	MenuHandler();

	void Add(std::string menuSectionID, HighlightableText menuText);
	bool SetActiveSection(std::string menuSectionID);
	std::vector<HighlightableText>* GetActiveSection() const;
	std::string GetActiveMenuSectionID() const;

protected:
	std::map<std::string, std::vector<HighlightableText>> m_menuStash;
	std::vector<HighlightableText> *pm_activeMenuSection = NULL;
	std::string m_activeMenuSectionID;
};

#endif // !MENU_HANDLER_H