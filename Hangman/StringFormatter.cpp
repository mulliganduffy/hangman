#include "stdafx.h"
#include "StringFormatter.h"
#include <iostream>

void strFormat::PrintHighlightedString(const HANDLE & handle, const HighlightableText & text, const bool & bNewLine)
{
	for (size_t i = 0; i < text.GetRawString().length(); i++) {
		WORD currentCharColor = text.GetColorForChar(i);
		SetConsoleTextAttribute(handle, currentCharColor);
		std::cout << text.GetRawString()[i];
	}
	SetConsoleTextAttribute(handle, 7);
	if (bNewLine) {
		std::cout << '\n';
	}
}

void strFormat::PrintHighlightedString(const HANDLE & handle, const std::vector<HighlightableText>& texts)
{
	for (HighlightableText text : texts) {
		PrintHighlightedString(handle, text);
	}
}

size_t strFormat::GetLongestStringInVector(const std::vector<HighlightableText>& lines)
{
	size_t longestLength = 0;
	for (HighlightableText thisLine : lines) {
		longestLength = (thisLine.GetRawString().length() > longestLength) ? thisLine.GetRawString().length() : longestLength;
	}
	return longestLength;
}

void strFormat::PrintMultipleHighlightedTextsInsideBox(const HANDLE & handle, const std::vector<HighlightableText>& texts, const BoxParameters & box)
{
	const int LONGEST_LENGTH = GetLongestStringInVector(texts);
	const int ROWS = 2 + texts.size() + (box.padding * 2);
	const int COLS = 2 + LONGEST_LENGTH + (box.padding * 2);
	int textIndex = 0;

	//Print each row
	for (int r = 0; r < ROWS; r++) {
		std::size_t c = 0;
		//Print out each column in this row
		while (c < COLS) {
			bool bOnString = ((box.padding + 1 <= r && r <= box.padding + texts.size()) && (c == box.padding + 1));
			if (bOnString) {
				PrintHighlightedString(handle, texts[textIndex], false);
				c += texts[textIndex].GetRawString().length();
				textIndex++;
			} else {
				bool bOnBorder = (r == 0 || r == ROWS - 1 || c == 0 || c == COLS - 1);
				char charToPrint = (bOnBorder) ? box.borderChar : ' ';
				WORD charColor = (bOnBorder) ? box.borderColor : box.backgroundColor;
				SetConsoleTextAttribute(handle, charColor);
				std::cout << charToPrint;
				c++;
			}
		}
		std::cout << '\n';
	}
	SetConsoleTextAttribute(handle, 7);
}
