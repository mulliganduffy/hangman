#include "stdafx.h"
#include "HangmanSession.h"
#include <ctime>
#include <fstream>
#include <map>
#include <algorithm>

HangmanSession::HangmanSession()
{
	srand(time(NULL));
	
	Reset();
}

std::string HangmanSession::GetSessionWord() const { return m_sessionWord; }
std::string HangmanSession::GetWordGuess() const { return m_wordGuess; }
std::string HangmanSession::GetResponseMessage() const { return m_responseMessage; }
HangmanGameState HangmanSession::GetGameState() const { return m_gameState; }

void HangmanSession::Reset()
{
	m_givenGuesses.clear();
	m_gameState = HangmanGameState::NoBody;
	m_responseMessage = "The game begins";

	ChooseWord();
}

void HangmanSession::ChooseWord()
{
	//Open up a file input stream to our stash of words
	std::ifstream fileInput("WordStash.txt");
	//Continue only if we can access the file
	if (fileInput.good()) {
		int wordCount = 0;
		std::map<int, std::string> availableWords;
		
		while (!fileInput.eof()) {
			std::string currentWord;
			std::getline(fileInput, currentWord);
			availableWords[wordCount] = currentWord;
			wordCount++;
		}

		//Pick a random word from the map
		int pickedNumber = rand() % wordCount;
		std::string chosenWord = availableWords[pickedNumber];
		m_sessionWord = chosenWord;
		m_wordGuess.assign(chosenWord.length(), '#');
	} else {
		m_sessionWord = "spaghetti";
		m_wordGuess.assign(m_sessionWord.length(), '#');
	}
}

void HangmanSession::ProcessGuess(std::string guessWord)
{
	std::transform(guessWord.begin(), guessWord.end(), guessWord.begin(), ::tolower);

	if (guessWord.compare(m_sessionWord) == 0)
	{
		m_wordGuess = guessWord;
	}
	else
	{
		//not correct word
		m_responseMessage = "You guessed wrong";
		UpdateGameState();
	}
	CreateListOfGivenGuesses();
}

void HangmanSession::ProcessGuess(char guess)
{
	char guessLowerCased = tolower(guess);
	
	//Only update wordGuess if a correct char, not entered before, has been inputted
	bool bIsValidGuess = true;

	//Check if the guess was already entered
	if (m_givenGuesses.count(guessLowerCased) != 0) {
		bIsValidGuess = false;
		m_responseMessage = "That letter was already entered";
	} else { //Register the guess as it is new
		m_givenGuesses[guessLowerCased] = true;
		if (m_sessionWord.find(guessLowerCased) == std::string::npos) {
			bIsValidGuess = false;
			m_responseMessage = "That was an incorrect guess";
			UpdateGameState();
		}
	}

	if (bIsValidGuess) {
		//Register the guess with all other correct guesses
		m_givenGuesses[guessLowerCased] = true;

		m_wordGuess.clear(); //Clear the string; it will be re-written
		for (char sessionChar : m_sessionWord) {
			char charToInsert = (m_givenGuesses.count(sessionChar) != 0) ? sessionChar : '#';
			m_wordGuess.push_back(charToInsert);
		}
		m_responseMessage = "Your guess is correct";
	}
	CreateListOfGivenGuesses();
}

void HangmanSession::UpdateGameState()
{
	//Each mistake the player makes will update the game state
	//The game state will be updated in the following order:
	//Head, Torso, Arms, and the Legs (Left then right in the case of arms/legs)
	if ((m_gameState & HangmanGameState::Head) != HangmanGameState::Head) {
		//Assume that if the 'Head' flag has not been set, that this is the player's first
		//mistake. I.E. Change the gameState from 'NoBody' as opposed to performing bitwise OR
		m_gameState = HangmanGameState::Head;
	} else if ((m_gameState & HangmanGameState::Torso) != HangmanGameState::Torso) {
		m_gameState = m_gameState | HangmanGameState::Torso;
	} else if ((m_gameState & HangmanGameState::LeftArm) != HangmanGameState::LeftArm) {
		m_gameState = m_gameState | HangmanGameState::LeftArm;
	} else if ((m_gameState & HangmanGameState::RightArm) != HangmanGameState::RightArm) {
		m_gameState = m_gameState | HangmanGameState::RightArm;
	} else if ((m_gameState & HangmanGameState::LeftLeg) != HangmanGameState::LeftLeg) {
		m_gameState = m_gameState | HangmanGameState::LeftLeg;
		m_responseMessage.append("\nLast guess make it count");
	} else if ((m_gameState & HangmanGameState::RightLeg) != HangmanGameState::RightLeg) {
		m_gameState = m_gameState | HangmanGameState::RightLeg;
	}
}

void HangmanSession::CreateListOfGivenGuesses()
{
	m_responseMessage.append("\nGuessed letters: ");
	for (auto &currentChar : m_givenGuesses) {
		m_responseMessage.push_back(currentChar.first);
		m_responseMessage.push_back(' ');
	}
	m_responseMessage.pop_back();
}

bool HangmanSession::IsGameFinished() const
{
	return (m_gameState == gameOver) || (m_sessionWord.compare(m_wordGuess) == 0);
}

void HangmanSession::EndSession(Player& player, bool& bHasWon)
{
	if (m_sessionWord.compare(m_wordGuess) == 0) {
		player.UpdateWinsAndLoses(true);
		bHasWon = true;
	} else if (m_gameState == gameOver) {
		player.UpdateWinsAndLoses(false);
		bHasWon = false;
	}

	Reset();
}
