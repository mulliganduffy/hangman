/* A namespace which contains functions for drawing 'pictures' onto the console screen 
 We decided to make it seperate from the rest of the logic in HangmanSession */
#pragma once
#ifndef HANGMAN_DRAWER_H
#define HANGMAN_DRAWER_H

#include "HangmanGameState.h"
#include <Windows.h>

namespace hangDraw {

	void DrawFullHangmanPicture(const HangmanGameState& state);
	void DrawHangmanPost();
	void DrawHangmanMan(const HangmanGameState& state);
	void DrawBodyPart(const SHORT& bufferXPos, const SHORT& bufferYPos, const SHORT& bufferWidth, const SHORT& bufferHeight);
}

#endif // !HANGMAN_DRAWER_H

