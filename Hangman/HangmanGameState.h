/* This enum is used to determine how close the player will be to loosing their current game
 This is achieved by using powers of two e.g. 2^1, 2^2, etc*/
#pragma once
#ifndef HANGMAN_GAME_STATE_H
#define HANGMAN_GAME_STATE_H

#include "EnumBitMask.h"

enum class HangmanGameState
{
	NoBody = 1,
	Head = 2,
	Torso = 4,
	LeftArm = 8,
	RightArm = 16,
	LeftLeg = 32,
	RightLeg = 64
};

//This will allow us to perform bitwise operations with this class
template<>
struct enable_bitmask_operators<HangmanGameState> {
	static constexpr bool enable = true;
};

#endif // !HANGMAN_GAME_STATE_H
